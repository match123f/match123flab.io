# 文档概述

!!! note "注意"
    本项目还处于活跃开发中。

欢迎来到Match123的个人博客。Match123会在这里投稿一些个人观点和学习经验。

[TOC]

## 项目信息
- 作者/贡献者：Match123
- 协议：[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0) 及其以后版本
- 源码地址：
	- [BitBucket](https://bitbucket.org/match123/self-website)
	- [Gitlab](https://gitlab.com/match123f/match123f.gitlab.io) (主要)
- 反馈地址：[GitLab Issues](https://gitlab.com/match123f/match123f.gitlab.io/-/issues)（不稳定）
- 构建地址：[Read the Docs](https://readthedocs.org/projects/match123/)

## 精神状态

- [x] 酩酊大醉
- [x] 就地躺平