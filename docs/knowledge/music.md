# 音乐知识

!!! warning "警告"
	本文档并不属于自由文档的一部分。以下引用的资源由数个出版社所有，本文档仅提供学习分享通道，不对下列内容享有任何权利。

这里列举了中考所需的音乐知识，以下内容均取自**湘文艺版**教材。本文档不提供教材的电子副本，请读者自行登录智慧教育平台查询。

 - [七年级上册](https://www.zxx.edu.cn/tchMaterial/detail?contentType=assets_document&contentId=89a20f12-26fb-420e-87bc-aa46e0863920&catalogType=tchMaterial&subCatalog=dzjc)
 - [七年级下册](https://www.zxx.edu.cn/tchMaterial/detail?contentType=assets_document&contentId=ce5915aa-90cb-4174-bd7f-976b14b16b24&catalogType=tchMaterial&subCatalog=dzjc)
 - [八年级上册](https://www.zxx.edu.cn/tchMaterial/detail?contentType=assets_document&contentId=e2d0db96-bf3c-4097-bda6-f681e903fcb7&catalogType=tchMaterial&subCatalog=dzjc)
 - [八年级下册](https://www.zxx.edu.cn/tchMaterial/detail?contentType=assets_document&contentId=1cd6ba3d-480f-4f1d-8ef8-e4791c5f63b6&catalogType=tchMaterial&subCatalog=dzjc)
 - [九年级上册](https://www.zxx.edu.cn/tchMaterial/detail?contentType=assets_document&contentId=ee6fafa5-1377-4051-b08a-5f72da1557de&catalogType=tchMaterial&subCatalog=dzjc)
 - [九年级下册](https://www.zxx.edu.cn/tchMaterial/detail?contentType=assets_document&contentId=be7671ec-f19d-43ec-9299-bd719d253318&catalogType=tchMaterial&subCatalog=dzjc)

