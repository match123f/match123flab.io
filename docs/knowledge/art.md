# 美术知识

!!! warning "警告"
	本文档并不属于自由文档的一部分。以下引用的资源由数个出版社所有，本文档仅提供学习分享通道，不对下列内容享有任何权利。

这里列举了中考所需的美术知识，以下内容均取自**湘文艺版**教材。本文档不提供教材的电子副本，请读者自行登录智慧教育平台查询。

 - [七年级上册](https://www.zxx.edu.cn/tchMaterial/detail?contentType=assets_document&contentId=7c12ff2a-acb0-4963-9547-98677fe4cb41&catalogType=tchMaterial&subCatalog=dzjc)
 - [七年级下册](https://www.zxx.edu.cn/tchMaterial/detail?contentType=assets_document&contentId=a6d8f086-14cc-403e-a7ee-7b2576beff55&catalogType=tchMaterial&subCatalog=dzjc)
 - [八年级上册](https://www.zxx.edu.cn/tchMaterial/detail?contentType=assets_document&contentId=55acbeba-4e9c-440a-9f1b-30f45c65d833&catalogType=tchMaterial&subCatalog=dzjc)
 - [八年级下册](https://www.zxx.edu.cn/tchMaterial/detail?contentType=assets_document&contentId=413d8803-496b-41e4-b012-4d162608661b&catalogType=tchMaterial&subCatalog=dzjc)
 - [九年级上册](https://www.zxx.edu.cn/tchMaterial/detail?contentType=assets_document&contentId=5a74348d-b863-44bb-95ec-5b24c55c4eff&catalogType=tchMaterial&subCatalog=dzjc)
 - [九年级下册](https://www.zxx.edu.cn/tchMaterial/detail?contentType=assets_document&contentId=f3643cdb-a1ce-404f-b4aa-54942dd77a70&catalogType=tchMaterial&subCatalog=dzjc)

