> There is no English translation avaliable. However, you can contribute one if you're an Chinese-English translator or a native speaker.

# Match123's Blog by MkDocs

## Introduction

This is a personal blog written by Markdown. It seems like a document and is published on [Read the Docs](http://match123.readthedocs.io/)(site url). The blog is just talking about _study experience_, _personal opinions_, _examination tutorials_ and more. 

Attention: There is no warranty for any contents.

## License

All of the contents is under the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). The main idea is that if you copy text from this site, you should note "origin ver. is written by Match123" and so on, and you should share your version under the same license "CC BY-SA 4.0 or later".

## Contribute

* E-mail at [Gitlab service desk](mailto:contact-project+match123f-match123f-gitlab-io-44415005-issue-@incoming.gitlab.com).
* visit [Gitlab issues](https://gitlab.com/match123f/match123f.gitlab.io/-/issues)

## Build it locally

For more information, see [example-mkdocs-basic](https://github.com/readthedocs-examples/example-mkdocs-basic).

	 # Install required Python dependencies (MkDocs etc.)
	 pip install -r docs/requirements.txt
	 # Run the mkdocs development server
	 mkdocs serve